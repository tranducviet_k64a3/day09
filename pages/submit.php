<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <?php
        $result = $_COOKIE["result"];
    ?>

    <div class="center flex-items-start">
        <div class="container mt-20">
            <h4>Điểm của bạn là: <span style="color:red"><?php echo $result?></span>/10</h4>
            <h4>Nhận xét: 
                <?php 
                    if ($result < 4) {
                        echo '<span style="color:red">Bạn quá kém, cần ôn tập thêm</span>';
                    }
                    if ($result >= 4 && $result <= 7) {
                        echo '<span style="color:yellow">Cũng bình thường</span>';
                    }
                    if ($result > 7) {
                        echo '<span style="color:green">Sắp sửa làm được trợ giảng lớp PHP</span>';
                    }
                ?>
            </h4>
        </div>
    </div>
</body>
</html>