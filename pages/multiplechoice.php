<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <?php
        include ("../variable.php");
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = $_COOKIE["result"];
            if($_POST['submit']) {
                $count = 0;
                foreach ($questions as $key=>$value) {
                    $count++;
                    if ($count < 6) continue;
                    if(isset($_POST["$key"]) && !is_null($_POST["$key"])){
                        if ($_POST["$key"] == $answers["ans".$count]) {
                            $result++;
                        }
                    }
                }  
            }
            setcookie("result", $result, time() + 300, "/");
            header("location: ./submit.php");
        }
    ?>

    <form action="" method="post">
        <div class="center flex-center">
            <div class="container">
                <h1>Toán học</h1>
                <div class="mt-20 px-20">
                    <?php 
                        $count = 0;
                        foreach ($questions as $ques=>$value) {
                            $count++;
                            if ($count<6) continue;
                            echo '<div class="mt-10">
                                    <label for=""> Câu '.$count.': '.$value.'</label><br>
                                    <input type="radio" name="'.$ques.'" value="a">'.$choices["choice1"].'<br>
                                    <input type="radio" name="'.$ques.'" value="b">'.$choices["choice2"].'<br>
                                    <input type="radio" name="'.$ques.'" value="c">'.$choices["choice3"].'<br>
                                    <input type="radio" name="'.$ques.'" value="d">'.$choices["choice4"].'<br>
                                </div>';
                        }
                    ?>
                </div>
                <div class="flex-end mt-20">
                    <input type="submit" name="submit" value="Nộp bài">
                </div>
            </div>
        </div>
    </form>
</body>
</html>