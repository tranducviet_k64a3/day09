<?php 
global $questions, $choices, $answers;

$questions = array(
    "ques1"=>"1+1=?",
    "ques2"=>"1*1=?",
    "ques3"=>"1/1=?",
    "ques4"=>"1*1+2=?",
    "ques5"=>"2*1=?",
    "ques6"=>"2-1=?",
    "ques7"=>"2/1=?",
    "ques8"=>"2+1=?",
    "ques9"=>"2+2=?",
    "ques10"=>"2+1*2=?",
);

$choices = array(
    "choice1"=>"1",
    "choice2"=>"2",
    "choice3"=>"3",
    "choice4"=>"4",
);

$answers = array(
    "ans1"=>"b",
    "ans2"=>"a",
    "ans3"=>"a",
    "ans4"=>"c",
    "ans5"=>"b",
    "ans6"=>"a",
    "ans7"=>"b",
    "ans8"=>"c",
    "ans9"=>"d",
    "ans10"=>"d",
);

?>